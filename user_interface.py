import datetime
from representation import Month

from user_input.user_input import UserInput


class UserInterface(UserInput):
    """ Responsible for getting and validating user input.

    """

    def __init__(self, workers_group):
        super().__init__()
        self._workpairs = workers_group.workpairs
        self.display_helper_table = True

    def prompt_user_for_data(self):

        month_instance = self._get_month_data_from_user_prompt()

        if self.display_helper_table:
            self._display_workpairs_table()
            workpair_number = self._get_workpair_number_from_user(
                len(self._workpairs)
            )
            return month_instance, workpair_number
        return month_instance, 1

    def _get_month_data_from_user_prompt(self):
        # date (month or month and year both) we wish to build tables for
        desired_date = self._get_desired_date_from_user()
        # Creating an instance of Month with desired month and year.
        # By default, if user omits year it is set to today's year.
        month = Month(desired_date[0], desired_date[1])
        # holidays that occurs during normal workdays (like 9-th of May)
        month.holidays = self._get_days_list_from_user()
        # Weekends that are actually a workdays by government decision.
        month.extra_workdays = self._get_days_list_from_user(msg_set='workdays')
        return month

    def _get_workpair_number_from_user(self, number_of_workpairs):
        """ Expects number of next work pair from user.

        Args:
            number_of_workpairs:
        """
        self.inp_msg = "Номер следующей группы дежурных: "
        self.err_msg = "Ошибка при вводе номера группы дежурных:"

        return self.prompt_for_integer_in_range(1, number_of_workpairs)

    def _display_workpairs_table(self):
        """ Prints mapping of number to workpair to stdout.

        Useful as on-screen help reference when inputting
        workpair's number to start schedule with.
        """
        for i, workpair in enumerate(self._workpairs):
            print("{0}  =>  {1}".format(i + 1, workpair).ljust(37))

    def _get_desired_date_from_user(self):
        """ Expects user to provide month or month and year both.

        If user provides only month returns (month, current year).
        Else returns (month, year provided by user).
        month is an integer in range [1,12].
        year is an integer in range [2,9999]
        """
        def valid_month(month):
            return 1 <= month <= 12

        def valid_year(year):
            return 2 <= year <= 9999

        self.inp_msg = "Месяц [год] для которого нужна таблица: " 
        self.err_msg = "Неверные данные: месяц [1,12], [год [2,9999]]:"

        while True:
            date = self.prompt_for_integer_list()
            
            if len(date) == 1 and valid_month(date[0]):
                return date[0], datetime.date.today().year
            elif len(date) == 2:
                if valid_month(date[0]) and valid_year(date[1]):
                    return date[0], date[1]

            print(self.err_msg, date)

    def _get_days_list_from_user(self, msg_set="holidays"):
        """ Expects user to input a list of integers in range [1,31].

        representing valid days. Prompts for user input until that
        criteria is met.
        """
        if msg_set == "workdays":
            self.inp_msg = "Дополнительные рабочие дни (через пробел): "
            self.err_msg = "Ошибка при вводе дополнительных рабочих дней:"
        else:
            self.inp_msg = "Праздничные дни (через пробел): "
            self.err_msg = "Ошибка при вводе праздничных дней:"

        return self.prompt_for_list_of_integers_in_range(1, 31)




