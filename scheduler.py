# -*- coding: utf-8 -*-
"""Tool for generating table for duty allocation in a class.

Creates table for given month and year and writes them in the Excel file.

Usage:
    duty_scheduler.py (-d | -a | -da) [-e] [-h | --help]

Options:
    duty_scheduler.py -h | --help       display this message and exit
    duty_scheduler.py -d                generate duty table and output preview to STDOUT
    duty_scheduler.py -a                generate attendance table and output preview to STDOUT
    duty_scheduler.py -e                write table to Excel file instead of STDOUT

"""

from docopt import docopt
from config.config import Config
from models import WorkerGroup

from user_interface import UserInterface
from representation import DutyTable, AttendanceTable, DutyTableExcel, DutyTableStdout, AttendanceTableExcel, \
    AttendanceTableStdout


class ApplicationInterface(object):
    def __init__(self, month_instance, worker_group, workpair_number):
        self._worker_group = worker_group
        self._workpair_number = workpair_number
        self.write_xls = False
        self._month_instance = month_instance

    def generate_duty_table(self):
        # TODO put starting_workpair_number in the table constructor

        if self.write_xls:
            excel_table = DutyTableExcel(self._month_instance,
                                         self._worker_group)
            excel_table.starting_workpair_number = self._workpair_number
            excel_table.write_table_to_file()
            return

        table = DutyTableStdout(self._month_instance,
                          self._worker_group)

        # number of pair which duty is next.
        # Uses number_of_workpairs for validation
        table.starting_workpair_number = self._workpair_number

        self._output_table(table)

    def generate_attendance_table(self):

        if self.write_xls:
            AttendanceTableExcel(self._month_instance,
                                 self._worker_group).write_table_to_file()
            return

        table = AttendanceTableStdout(self._month_instance,
                                      self._worker_group)

        self._output_table(table)

    def _output_table(self, table_instance):
        if self.write_xls:
            table_instance.write_table_to_file()
        else:
            print(table_instance)


def main(args):
    """Provides CLI."""
    worker_group = WorkerGroup()

    # for command line version
    ui = UserInterface(worker_group)
    ui.display_helper_table = args['-d']
    month, workpair_number = ui.prompt_user_for_data()
    api = ApplicationInterface(month, worker_group, workpair_number)

    if args['-e']:
        api.write_xls = True

    if args['-d']:
        api.generate_duty_table()

    if args['-a']:
        api.generate_attendance_table()

if __name__ == "__main__":
    doc_args = docopt(__doc__, version='Slaves 0.2.2')
    main(doc_args)
