import unittest
import datetime
import os
import sys
from representation import Month

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
print(parentdir)
sys.path.insert(0,parentdir)
from scheduler import *
from models import Person, WorkerGroup
from config.config import Config

class MonthTest(unittest.TestCase):
    def setUp(self):
        self.s = Month(2, 2012)
        self.s1 = Month(12, 2012)
        self.s2 = Month(5, 2013)

    def test_gen_days_for_month(self):
        self.assertEquals(self.s._all_days_numbers(), list(range(1, 30)))
        self.assertEquals(self.s1._all_days_numbers(), list(range(1, 32)))

    def test_is_a_workday(self):
        self.assertTrue(self.s2._is_a_workday(17))
        self.assertFalse(self.s2._is_a_workday(18))
        self.assertFalse(self.s2._is_a_workday(19))
        self.s2.holidays = [9]
        self.assertFalse(self.s2._is_a_workday(9))
        self.s2.extra_workdays = [11]
        self.assertTrue(self.s2._is_a_workday(11))
        # if day is both in holidays and extra_workdays,
        # it is considered as workday
        self.s2.holidays = [10]
        self.s2.extra_workdays = [10]
        self.assertTrue(self.s2._is_a_workday(10))

    def test_remove_weekends(self):
        self.assertEquals(self.s._remove_weekends(), 
                [1,2,3,6,7,8,9,10,13,14,15,16,17,20,21,22,23,24,27,28,29])
        self.s.holidays = [2, 3]
        self.assertEquals(self.s._remove_weekends(), 
                [1,6,7,8,9,10,13,14,15,16,17,20,21,22,23,24,27,28,29])

    def test_all_workdays(self):
        self.assertEquals(self.s.workdays,
                [1,2,3,6,7,8,9,10,13,14,15,16,17,20,21,22,23,24,27,28,29])
        self.s.holidays = [2, 3]
        self.assertEquals(self.s.workdays,
                [1,6,7,8,9,10,13,14,15,16,17,20,21,22,23,24,27,28,29])
        self.s.extra_workdays = [11]
        self.assertEquals(self.s.workdays,
                [1,6,7,8,9,10,11,13,14,15,16,17,20,21,22,23,24,27,28,29])

    def test_weekday_for_day_number(self):
        feb = Month(2, 2014)
        self.assertEquals(feb._weekday_name(1), 'Сб')
        self.assertEquals(feb._weekday_name(2), 'Вс')
        self.assertEquals(feb._weekday_name(3), 'Пн')
        self.assertEquals(feb._weekday_name(4), 'Вт')
        self.assertEquals(feb._weekday_name(5), 'Ср')
        self.assertEquals(feb._weekday_name(6), 'Чт')
        self.assertEquals(feb._weekday_name(7), 'Пт')

    def test_all_workdays_2(self):
        feb = Month(2, 2014)
        feb.holidays = [24]

        self.assertEquals(feb.workdays,
                          [3,4,5,6,7,10,11,12,13,14,17,18,19,20,21,25,26,27,28])
        self.assertEquals(feb.workdays_weekdays,
                          ['Пн', 'Вт', 'Ср', 'Чт', 'Пт',
                           'Пн', 'Вт', 'Ср', 'Чт', 'Пт',
                           'Пн', 'Вт', 'Ср', 'Чт', 'Пт',
                           'Вт', 'Ср', 'Чт', 'Пт'])


class UserInterfaceTest(unittest.TestCase):
    def setUp(self):
        class UserInterfaceSeam(UserInterface):
            def _get_user_input(self, *args):
                for arg in args:
                    yield arg

        worker_group = WorkerGroup()
        self.ui = UserInterfaceSeam(worker_group)

    def tearDown(self):
        self.ui = None
    
    def test_get_desired_date_from_user(self):
        """ Should accept number in range [1,12]
        or two numbers in range [1,12] and [2, 9999]
        and return a tuple of number(s)
        If input input doesn't meet requirements, 
        should prompt another input.
        """
        
        a = self.ui._get_user_input("5", "sb 45", "11 2010 3", "4 1", "12 2012", "dger", "345", "5 10001", "5 2013")

        self.ui._get_user_input = lambda: a

        self.assertEqual(self.ui._get_desired_date_from_user(), (5, datetime.date.today().year))
        self.assertEqual(self.ui._get_desired_date_from_user(), (12, 2012))
        self.assertEqual(self.ui._get_desired_date_from_user(), (5, 2013))

    def test_get_days_list_from_user(self):
        """
        Should accept string containing numbers in range [1,31]
        separated by whitespaces and return a list of these numbers.
        If input doesn't meet these requirements,
        should prompt for another input.
        """
        
        a = self.ui._get_user_input("4, 5, 6", "9", "43 3 2 4", "1 2 3", "9 10", "17 1 0", "-2 2 3", "fdg drf", "dkk", "4")

        self.ui._get_user_input = lambda: a

        self.assertEqual(self.ui._get_days_list_from_user(), [9])
        self.assertEqual(self.ui._get_days_list_from_user(), [1,2,3])
        self.assertEqual(self.ui._get_days_list_from_user(), [9,10])
        self.assertEqual(self.ui._get_days_list_from_user(), [4])

    def test_get_workpair_from_user(self):
        """
        Should accept string of single number in range [1, number of workpairs]
        and return this number as integer.
        If input doesn't meet these requirements,
        should prompt for another input.
        """
        
        working_pair_number = 9

        a = self.ui._get_user_input("12 14", "abc", "bf rf", "2.0", "-1", "14", "1", "9")
        self.ui._get_user_input = lambda : a

        self.assertEqual(self.ui._get_workpair_number_from_user(working_pair_number),1)
        self.assertEqual(self.ui._get_workpair_number_from_user(working_pair_number),9)


class DutyTableTest(unittest.TestCase):
    def setUp(self):
        month = Month(5, 2013)
        month.holidays = list(range(1, 20)) + list(range(25, 32))
        self.worker_group = WorkerGroup()
        self.worker_group.workers = [Person("1", "FirstName1 LastName1"),
                                     Person("2", "FirstName2 LastName2"),
                                     Person("3", "FirstName3 LastName3"),
                                     Person("4", "FirstName4 LastName4"),
                                     Person("5", "FirstName2 LastName2"),
                                     Person("6", "FirstName3 LastName3"),
                                     Person("7", "FirstName4 LastName4"),
                                     Person("8", "FirstName4 LastName4"),
                                     ]
        self.worker_group.workpairs = [("FirstName1 LastName1", "FirstName2 LastName2"),
                                       ("FirstName3 LastName3", "FirstName5 LastName5"),
                                       ("FirstName4 LastName4", "FirstName7 LastName7"),
                                       ("FirstName8 LastName8", "FirstName6 LastName6"),
                                       ]
        self.w = DutyTable(month, self.worker_group)

    def tearDown(self):
        self.w = None

    def test_gen_workday_string(self):
        """Should return string of workdays. """
        self.assertEqual(self.w._gen_workday_string(),
                         " 20 | 21 | 22 | 23 | 24 |")

    def test_gen_workday_weekday_string(self):
        self.assertEquals(self.w._gen_workday_weekday_string(),
                          " Пн | Вт | Ср | Чт | Пт |")
    
    def test_next_workpair_index(self):
        """
        Should set starting_workpair_number to (starting_workpair_number - 1)
        """
        self.w.starting_workpair_number = 5
        self.assertEqual(self.w._starting_workpair_index, 4)

    def test_gen_duty_array(self):
        """ 
        Should return array of (workpairs x workdays) with 
        " xx " if some workpair is on duty on some workday and
        "    " if it's not.
        """
        self.w._starting_workpair_index = 0
        self._workdays = [1,2,3,4,5]
        self.assertEqual(self.w._gen_duty_array(),
                         [[True, False, False, False, True],
                          [False, True, False, False, False],
                          [False, False, True, False, False],
                          [False, False, False, True, False]])


class DutyTableExcelTest(unittest.TestCase):
    def setUp(self):
        month = Month(5, 2013)
        month.holidays = list(range(1, 20)) + list(range(25, 32))
        self.worker_group = WorkerGroup()
        self.worker_group.workers = [Person("1", "FirstName1 LastName1"),
                                     Person("2", "FirstName2 LastName2"),
                                     Person("3", "FirstName3 LastName3"),
                                     Person("4", "FirstName4 LastName4"),
                                     Person("5", "FirstName2 LastName2"),
                                     Person("6", "FirstName3 LastName3"),
                                     Person("7", "FirstName4 LastName4"),
                                     Person("8", "FirstName4 LastName4"),
                                     ]
        self.worker_group.workpairs = [("FirstName1 LastName1", "FirstName2 LastName2"),
                                       ("FirstName3 LastName3", "FirstName5 LastName5"),
                                       ("FirstName4 LastName4", "FirstName7 LastName7"),
                                       ("FirstName8 LastName8", "FirstName6 LastName6"),
                                       ]
        self.w = DutyTable(month, self.worker_group)

    def test_detect_week_borders(self):
        m = Month(2, 2014)
        d = DutyTableExcel(m, self.worker_group)

        self.assertEquals(d._detect_week_border(), {5, 10, 15})


class AttendanceTableTest(unittest.TestCase):
    def setUp(self):
        month = Month(5, 2013)
        month.holidays = list(range(1, 20)) + list(range(25, 32))
        workers = [Person("1", "Once Had"),
                   Person("2", "Dog Verbic"),
                   Person("3", "Best Friend"),
                   Person("4", "Ever Met")]
        workpairs = [("Once Had", "Dog Verbic"),
                     ("Best Friend", "Ever Met")]
        self.worker_group = WorkerGroup()
        self.worker_group.workers = workers
        self.a = AttendanceTable(month, self.worker_group)

    def tearDown(self):
        self.a = None

    def test_gen_attendance_array(self):
        # self.a.students = [Person('1','Once Had'), Person('2','Dog Verbic'),
        #                    Person('3','Best Friend'), Person('4','Ever Met')]
        # st = self.a.students
        self.assertEqual(self.a._gen_attendance_array(),
                [['№',' Фамилия','20', '21', '22', '23', '24'],
                 [self.worker_group.workers[0].number,' ' + self.worker_group.workers[0].name,'  ','  ','  ','  ','  '],
                 [self.worker_group.workers[1].number,' ' + self.worker_group.workers[1].name,'  ','  ','  ','  ','  '],
                 [self.worker_group.workers[2].number,' ' + self.worker_group.workers[2].name,'  ','  ','  ','  ','  '],
                 [self.worker_group.workers[3].number,' ' + self.worker_group.workers[3].name,'  ','  ','  ','  ','  ']])


class WorkerGroupTest(unittest.TestCase):
    
    def setUp(self):
        self.wg = WorkerGroup(workers_config="testing/test_students_config.cfg",
                              workpairs_config="testing/test_workpairs_config.cfg")

    def tearDown(self):
        self.wg = None

    def test_read_students_from_config_file_happy_path(self):
        self.assertEquals(
                self.wg._read_workers_from_config(),
                [("1", "Алабама Наталья"),
                 ("2", "Кнут Мария"),
                 ("3", "Мейер Петр"),
                 ("4", "Язь Александр")])

    # def test_read_workers_from_config_file_sad_path(self):
    #
    #     # broken_wg = WorkerGroup(workers_config="testing/no_such_file.cfg")
    #     with self.assertRaises(IOError):
    #         broken_wg = WorkerGroup(workers_config="testing/no_such_file.cfg")


    def test_read_workpairs_from_config_file_happy_path(self):
        self.assertEquals(
            self.wg._read_workpairs_from_config_file(),
            [("FirstName1 LastName1", "FirstName2 LastName2"),
             ("FirstName3 LastName3", "FirstName4 LastName4")]
        )

if __name__== "__main__":
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(MonthTest))
    suite.addTest(unittest.makeSuite(UserInterfaceTest))
    suite.addTest(unittest.makeSuite(DutyTableTest))
    suite.addTest(unittest.makeSuite(AttendanceTableTest))
    suite.addTest(unittest.makeSuite(WorkerGroupTest))
    #suite.addTest(unittest.makeSuite(IntegrationTests))
    unittest.TextTestRunner(verbosity=2).run(suite)
