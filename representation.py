import datetime
import calendar
import random
from abc import ABCMeta, abstractmethod

from xlwt3 import Workbook, easyxf


class StdoutOut(metaclass=ABCMeta):

    @abstractmethod
    def _gen_table_stdout(self):
        pass

    def __str__(self):
        return self._gen_table_stdout()


class ExcelOut(metaclass=ABCMeta):
    def __init__(self, output_file_name):
        self._output_file = output_file_name
        self._workbook = Workbook()
        self._sheet = None

    def _add_sheet(self, name):
        self._sheet = self._workbook.add_sheet(name)

    def _cell_style(self, left='thin', right='thin', top='thin',
                    bottom='thin', height='240', bold='False', hor_al='center'):
        """ Sets style for Excel cell.

        """
        return ('font: name Arial, height {4}, bold {5};'
                'borders: left {0}, right {1}, top {2}, bottom {3};'
                'alignment: horizontal {6}, vertical center'.format(
                left, right, top, bottom, height, bold, hor_al))

    @abstractmethod
    def write_table_to_file(self):
        """ Writes file to disk. """


class DutyTable(object):
    def __init__(self, month_instance, worker_group):
        self._month = month_instance
        self._wg = worker_group
        self._starting_workpair_index = 1
        self.next_workpair_number = self._starting_workpair_index - 1

    @property
    def starting_workpair_number(self):
        """ Returns an index of next workpair in workpairs list.

        """
        return self._starting_workpair_index

    @starting_workpair_number.setter
    def starting_workpair_number(self, workpair_number):
        """ Sets next workpair number.

        Args:
            workpair_number: an integer representing a number in workpairs table

            Users of this program are not accustomed to enumerate values starting
            with 0. They are used to
        """
        self._starting_workpair_index = workpair_number - 1

    def _gen_duty_array(self):
        """ Returns array with duty schedule of pairs marked as " xx " and others as "    ".

        Creates two-dimentional
        array of (workdays X number of pairs) number_of_workpairs and marks next_workpair
        as " xx "
        """

        return ([[self._workpair_on_duty(workday_number, workpair_number)
                  for workday_number, __ in enumerate(self._month.workdays)]
                  for workpair_number, __ in enumerate(self._wg.workpairs)])

    def _workpair_on_duty(self, workday_num, workpair_num):
        return workpair_num == (self._starting_workpair_index + workday_num) % self._wg.number_of_workpairs

    def _gen_workday_string(self):
        """ Returns formatted string of workdays.

        Example: [1,2,3] => " 01 | 02 | 03 |"
        """
        return "".join([" {0} |".format(str(workday).zfill(2))
                        for workday in self._month.workdays])

    def _gen_workday_weekday_string(self):
        """ Returns formatted string of day's names."""
        return "".join([" {0} |".format(str(workday))
                        for workday in self._month.workdays_weekdays])


class DutyTableStdout(DutyTable, StdoutOut):
    def _gen_table_stdout(self):
        workdays_string = self._gen_workday_string()
        workday_weekdays_string = self._gen_workday_weekday_string()
        pretty_array = [[' xx |' if mark else '    |'
                       for mark in row]
                       for row in self._gen_duty_array()]

        thick_line = "=" * (41 + len(workdays_string))
        thin_line = "-" * (41 + len(workdays_string))

        # Constructing table by parts as a string.
        title = ''.join(["\n", "График дежурств на {} {}".format(
                         self._month.name, self._month.year).center(130),
                         "\n\n", thick_line, "\n"])

        date_row = ''.join(["| ", "Дежурные / Дата".center(37),
                            " |", workdays_string, "\n"])

        weekday_row = ''.join(["| ", "День недели".center(37),
                               " |", workday_weekdays_string, "\n", thick_line, "\n"])

        pretty_table = [title, date_row, weekday_row]

        for p in range(self._wg.number_of_workpairs):
            pretty_table.append(''.join(["| ",
                                "{}, {}".format(*self._wg.workpairs[p]).center(37),
                                " |", "".join(pretty_array[p]), "\n"]))

            pretty_table.append(''.join([thick_line, "\n"])
                                if p == self._wg.number_of_workpairs - 1
                                else ''.join([thin_line, "\n"]))

        return ''.join(pretty_table)


class DutyTableExcel(DutyTable, ExcelOut):

    def __init__(self, month_instance, worker_group):
        DutyTable.__init__(self, month_instance, worker_group)
        ExcelOut.__init__(self,
                          "./tables/duty_table_{0}_{1}.xls"
                          .format(self._month.name, self._month.year))
        self.PARAMS = {
            "title_font_height": '320',
            "header_row_height": 500,
            "duty_row_height": 900,
            "names_width": 12000,
            "dates_width": 1000,
            }

    def write_table_to_file(self):
        """ Combines workpairs and workdays into the table and writes to xls file

        style variable controls the style of the cell.
        """
        self._add_sheet('Duty table')

        # Attempting to calculate week borders' index
        week_borders = self._detect_week_border()

        # Makes it easier to track row number if need to add something in the middle.
        row_offset = 0

        self._write_title(row_offset)

        self._write_duty_header(row_offset + 2)
        self._write_workdays(row_offset + 2, week_borders)

        self._write_weekday_header(row_offset + 3)
        self._write_workdays_weekdays(row_offset + 3, week_borders)

        self._write_duty_markup(row_offset + 4, week_borders)

        self._workbook.save(self._output_file)

    def _write_title(self, current_row):
        column = 7
        height = self.PARAMS["title_font_height"]
        style = self._cell_style(left='no_line', right='no_line', top='no_line',
                                 bottom='no_line', height=height,
                                 bold='True')
        self._sheet.write(current_row, column, "График дежурств на {0} {1}".format(
            self._month.name.lower(),
            self._month.year), easyxf(style))

    def _write_duty_header(self, current_row):
        column = 0
        style = self._cell_style(left='thick', right='thick',
                                 top='thick', bottom='no_line', bold='True')
        self._sheet.write(current_row, column, "Дежурные/дата", easyxf(style))
        # Writing workdays' dates.
        self._sheet.row(current_row).height = self.PARAMS['header_row_height']

    def _write_workdays(self, current_row, week_borders):
        for j in range(self._month.number_of_workdays):
            self._sheet.col(j + 1).width = self.PARAMS['dates_width']

            if j == 0:
                style = self._cell_style(left='thick', top='thick',
                                         bottom='no_line')
            elif j == self._month.number_of_workdays - 1:
                style = self._cell_style(right='thick', top='thick',
                                         bottom='no_line')
                if j in week_borders:
                    style = self._cell_style(right='thick', top='thick',
                                             bottom='no_line', left='thick')
            else:
                style = self._cell_style(top='thick', bottom='no_line')
                if j in week_borders:
                    style = self._cell_style(top='thick', bottom='no_line',
                                             left='thick')

            self._sheet.write(current_row, j + 1, str(self._month.workdays[j]), easyxf(style))

    def _write_weekday_header(self, current_row):
        column = 0
        style = self._cell_style(left='thick', right='thick',
                                 bottom='thick', top='no_line', bold='True')
        self._sheet.row(current_row).height = 500
        self._sheet.write(current_row, column, "День недели", easyxf(style))

    def _write_workdays_weekdays(self, current_row, week_borders):
        self._sheet.row(current_row).height_mismatch = 1
        for j in range(self._month.number_of_workdays):
            self._sheet.col(j + 1).width = self.PARAMS['dates_width']

            if j == 0:
                style = self._cell_style(left='thick', top='no_line',
                                         bottom='thick')
            elif j == self._month.number_of_workdays - 1:
                style = self._cell_style(right='thick', top='no_line',
                                         bottom='thick')
                if j in week_borders:
                    style = self._cell_style(right='thick', top='no_line',
                                             bottom='thick', left='thick')
            else:
                style = self._cell_style(top='no_line', bottom='thick')
                if j in week_borders:
                    style = self._cell_style(top='no_line', bottom='thick',
                                             left='thick')

            self._sheet.write(current_row, j + 1, str(self._month.workdays_weekdays[j]), easyxf(style))

    def _write_duty_markup(self, current_row, week_borders):
        self._sheet.col(0).width = self.PARAMS['names_width']
        duty_array = self._gen_duty_array()
        colours = ('green', 'indigo', 'aqua')

        for p in range(self._wg.number_of_workpairs):
            self._sheet.row(p + current_row).height_mismatch = 1
            self._sheet.row(p + current_row).height = self.PARAMS['duty_row_height']

            if p == self._wg.number_of_workpairs - 1:
                style = self._cell_style(left='thick', right='thick',
                                         bottom='thick')
            else:
                style = self._cell_style(left='thick', right='thick')

            self._sheet.write(p + current_row,
                              0,
                              "{0}, {1}".format(*self._wg.workpairs[p]),
                              easyxf(style))

            # writing markdown for pairs on duty
            for j in range(self._month.number_of_workdays):

                # Bottom right corner.
                if p == self._wg.number_of_workpairs - 1 and j == self._month.number_of_workdays - 1:
                    style = self._cell_style(right='thick', bottom='thick')
                    if j in week_borders:
                        style = self._cell_style(right='thick', bottom='thick',
                                                 left='thick')

                elif j == self._month.number_of_workdays - 1:
                    style = self._cell_style(right='thick')
                    if j in week_borders:
                        style = self._cell_style(right='thick', left='thick')

                # Bottom row.
                elif p == self._wg.number_of_workpairs - 1:
                    style = self._cell_style(bottom='thick')
                    if j in week_borders:
                        style = self._cell_style(bottom='thick', left='thick')
                else:
                    style = self._cell_style()
                    if j in week_borders:
                        style = self._cell_style(left='thick')

                if duty_array[p][j]:
                    pattern = (style + "; " +
                               'pattern: pattern solid_fill, fore_colour {0}'.format(
                                   random.choice(colours)))
                else:
                    pattern = style

                self._sheet.write(p + current_row, j + 1, '    ', easyxf(pattern))

    def _detect_week_border(self):
        """Return indexes in the list of workdays if date's difference is greater than 1.

        """
        return set([i for i, day in enumerate(self._month.workdays)
                    if day - self._month.workdays[i - 1] > 1])


class AttendanceTable(object):
    def __init__(self, month_instance, worker_group):
        self._month = month_instance
        self._wg = worker_group

    def _gen_attendance_array(self):
        attendance_array = [['  '] * (self._month.number_of_workdays + 2)
                            for _ in range(self._wg.number_of_workers + 1)]

        attendance_array[0][0] = '№'
        attendance_array[0][1] = ' Фамилия'

        # fill first row with workdays numbers starting with third column
        for col in range(2, self._month.number_of_workdays + 2):
            attendance_array[0][col] = str(self._month.workdays[col - 2])

        # fill first and second columns with numbers and names of workers
        # starting with second row
        for row in range(1, self._wg.number_of_workers + 1):
            attendance_array[row][0] = self._wg.workers[row - 1].number
            attendance_array[row][1] = ''.join([' ', self._wg.workers[row - 1].name])

        return attendance_array


class AttendanceTableStdout(AttendanceTable, StdoutOut):
    def _gen_table_stdout(self):
        at_ar = self._gen_attendance_array()[:]
        # adding some decorations to the table
        for i in range(self._wg.number_of_workers + 1):
            for j in range(self._month.number_of_workdays + 2):
                if i == 0:
                    if j == 0:
                        at_ar[i][j] = ''.join(['| ', at_ar[i][j], '  |'])
                    elif j == 1:
                        at_ar[i][j] = ''.join([at_ar[i][j].ljust(19), ' |'])
                    else:
                        at_ar[i][j] = ''.join([' ', at_ar[i][j].zfill(2), ' |'])
                else:
                    if j == 0:
                        at_ar[i][j] = ''.join(['| ', at_ar[i][j].zfill(2), ' |'])
                    elif j == 1:
                        at_ar[i][j] = ''.join([at_ar[i][j].ljust(19), ' |'])
                    else:
                        at_ar[i][j] = ''.join([' ', at_ar[i][j], ' |'])

        thick_line = '=' * (27 + 5 * self._month.number_of_workdays)
        thin_line = '-' * (27 + 5 * self._month.number_of_workdays)

        pretty_table = ["\n",
                        "Таблица посещаемости на {} {}".format(
                            self._month.name.lower(),
                            self._month.year).center(130),
                        "\n\n",
                        thick_line,
                        "\n"]

        for i, row in enumerate(at_ar):
            pretty_table.append(''.join([''.join(row),
                                         '\n',
                                         thick_line if i == 0 or i == len(at_ar) - 1 else thin_line,
                                         '\n']))

        return ''.join(pretty_table)


class AttendanceTableExcel(AttendanceTable, ExcelOut):
    def __init__(self, month_instance, worker_group):
        super().__init__(month_instance, worker_group)
        self._output_file = "./tables/attendance_table_{0}_{1}.xls".format(
            self._month.name, self._month.year)

    def write_table_to_file(self):
        """ Combines students and workdays into table and writes to xls file.

        style variable controls the style of the cell
        """
        at_ar = self._gen_attendance_array()

        book = Workbook()
        sheet = book.add_sheet('Attendance')
        # excel's cells params
        height = 1000
        names_width = 6100
        dates_width = 1500

        # head of the table
        sheet.row(0).height = 500
        style = self._cell_style(left='no_line', right='no_line',
                                 top='no_line', bottom='no_line', height='320', bold='True')
        sheet.write(0, 9, "Таблица посещаемости на {0} {1}".format(
            self._month.name.lower(),
            self._month.year), easyxf(style))

        # filling the table
        sheet.col(0).width = dates_width
        sheet.col(1).width = names_width
        for i in range(len(at_ar)):
            sheet.row(i + 2).height_mismatch = 1
            sheet.row(i + 2).height = height
            for j in range(len(at_ar[0])):
                if j > 1:
                    sheet.col(j).width = dates_width
                    # top row
                if i == 0:
                    if j == 0:
                        style = self._cell_style(left='thick', top='thick',
                                                 bottom='thick')
                    elif j == 1 or j == len(at_ar[0]) - 1:
                        style = self._cell_style(right='thick', top='thick',
                                                 bottom='thick')
                    else:
                        style = self._cell_style(top='thick', bottom='thick')
                # bottom row
                elif i == len(at_ar) - 1:
                    if j == 0:
                        style = self._cell_style(left='thick', bottom='thick')
                    elif j == 1:
                        style = self._cell_style(right='thick',
                                                 bottom='thick', hor_al='left')
                    elif j == len(at_ar[0]) - 1:
                        style = self._cell_style(right='thick', bottom='thick')
                    else:
                        style = self._cell_style(bottom='thick')
                # other rows
                else:
                    if j == 0:
                        style = self._cell_style(left='thick')
                    elif j == 1 or j == len(at_ar[0]) - 1:
                        style = self._cell_style(right='thick', hor_al='left')
                    else:
                        style = self._cell_style()

                sheet.write(i + 2, j, at_ar[i][j], easyxf(style))

        book.save(self._output_file)


class Month(object):
    """ Represents month we generating data for.

    Attributes:
        holidays: A list of integers representing holidays for given month
        extra_workdays: A list of integers representing sundays and saturdays
                        that are actually a workdays
        month: An integer representing month we will generate data for
        year: An integer representing year we will generate data for
                      By default it is today's year
    """

    def __init__(self, month, year=None):
        self.number = month
        self.name_to_number_map = self._generate_month_dic()
        self.name = self.name_to_number_map[self.number]
        self.year = year if year else datetime.date.today().year
        self.holidays = []
        self.extra_workdays = []
        self._date_to_day_map = {0: "Пн",
                                 1: "Вт",
                                 2: "Ср",
                                 3: "Чт",
                                 4: "Пт",
                                 5: "Сб",
                                 6: "Вс"}

    @property
    def workdays(self):
        """ Returns an int list of workdays for month.

        """
        return self._remove_weekends()

    @property
    def number_of_workdays(self):
        return len(self.workdays)

    @property
    def workdays_weekdays(self):
        return [self._weekday_name(day_number)
                for day_number in self.workdays]

    def _remove_weekends(self):
        return list(filter(self._is_a_workday, self._all_days_numbers()))

    def _is_a_workday(self, day):

        if day in self.extra_workdays:
            return True

        weekends = {5, 6} # saturday and sunday
        weekday = self._weekday_number(day)

        return (weekday not in weekends) and (day not in self.holidays)

    def _weekday_number(self, day):
        return datetime.date(self.year, self.number, day).weekday()

    def _weekday_name(self, day_number):
        return self._date_to_day_map[self._weekday_number(day_number)]

    def _all_days_numbers(self):
        """Returns a list of day's numbers for this month. """

        _, number_of_days = calendar.monthrange(self.year, self.number)

        return [day + 1 for day in range(number_of_days)]

    def _generate_month_dic(self):
        return dict(zip(
            [x for x in range(1, 13)],
            ["Январь", "Февраль", "Март", "Апрель",
             "Май", "Июнь", "Июль", "Август",
             "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]))




