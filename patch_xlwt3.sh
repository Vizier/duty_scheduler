#!/bin/bash

# NOTE change path to point to xlwt3/formula.py on your system
PATH_TO_FORMULA_PY=~/.virtualenvs/duty-scheduler/lib/python3.4/site-packages/xlwt3/formula.py
patch $PATH_TO_FORMULA_PY < formula_patch.txt
