from models import Person


class Config(object):
    def __init__(self):
        self._students_config = "config/workers.cfg"
        self._workpairs_config = "config/workpairs.cfg"

    def get_workers(self):
        # return self._read_workers_from_config()
        return [Person(line[0], line[1])
                for line in self._read_workers_from_file()]

    def get_workpairs(self):
        return self._read_workpairs_from_file()

    def _read_workers_from_file(self):
        with open(self._students_config, 'r') as f:
            return [(str(num + 1), name.strip())
                    for (num, name) in enumerate(f)]

    def _read_workpairs_from_file(self):
        with open(self._workpairs_config, 'r') as f:
            return [(l.split(',')[0].strip(), l.split(',')[1].strip()) for l in f]



