import sys


class WorkerGroup(object):
    def __init__(self, workers_config="config/workers.cfg",
                 workpairs_config="config/workpairs.cfg"):
        self._workers_config = workers_config
        self._workpairs_config = workpairs_config
        self.workers = self._init_workers()
        self.workpairs = self._init_workpairs()

    @property
    def number_of_workpairs(self):
        return len(self.workpairs)

    @property
    def number_of_workers(self):
        return len(self.workers)

    def _init_workers(self):
        return [Person(line[0], line[1])
                for line in self._read_workers_from_config()]

    def _init_workpairs(self):
        return self._read_workpairs_from_config_file()

    def _read_workers_from_config(self):
        try:
            with open(self._workers_config, 'r') as f:
                names_list = sorted([name.strip() for name in f])
            return [(str(i + 1), name) for i, name in enumerate(names_list)]
        except IOError as e:
            print("Список рабочих не найден.")
            print(e)

    def _read_workpairs_from_config_file(self):
        with open(self._workpairs_config, 'r') as f:
            return [(l.split(',')[0].strip(), l.split(',')[1].strip()) for l in f]


class Person(object):
    def __init__(self, number, name):
        self.number = number
        self.name = name


class WorkPair(object):
    def __init__(self, arg):
        self.first = arg
        

