import unittest
from .user_input import UserInput, Range, RangeError


class GetValuesTest(unittest.TestCase):
    def setUp(self):
        # instead of testing GetValues class we will test it's
        # subclass with _get_user_input method overriden to
        # create a seam and not be dealing with actual user inputs
        class GetValuesSeem(UserInput):
            def _get_user_input(self, *args):
                """
                Method to simulate user inputs. Overrides private method
                from GetValues class for testing purposes. Takes a bunch
                of values and yields them one at a time to simulate user
                putting correct and incorrect values (validation methods
                run in a while loops).
                """
                for arg in args:
                    yield arg

        self.v = GetValuesSeem(testing=True)

    def TearDown(self):
        self.v.dispose()
        self.v = None

    def user_input(self, *args):
        for arg in args:
            yield arg
            
    def test_get_number(self):
        a = (self.v._get_user_input('ab',
                                    '34 56',
                                    '[34]',
                                    '42',
                                    '-9',
                                    '34 56',
                                    '[34]',
                                    'ab',
                                    '32.3 34.3',
                                    '34.1 34.2',
                                    '56.7',
                                    'ab',
                                    '32.3 34.3',
                                    '34.1 34.2',
                                    '-56.7',
                                    ))

        self.v._get_user_input = lambda: a

        self.assertEqual(self.v._get_number(int), 42)
        self.assertEqual(self.v._get_number(int), -9)
        self.assertEqual(self.v._get_number(float), 56.7)
        self.assertEqual(self.v._get_number(float), -56.7)

    def test_get_list(self):

        a = self.v._get_user_input("12 34 56 76", "", "This is a list of strings")

        self.v._get_user_input = lambda: a

        self.assertEqual(self.v._get_list(), ['12', '34', '56', '76'])
        self.assertEqual(self.v._get_list(), [])
        self.assertEqual(self.v._get_list(), ["This", "is", "a", "list", "of", "strings"])

    def test_expand_dashed_range(self):
        self.assertEqual(self.v._expand_dashed_range('1-4'), [1, 2, 3, 4])

    def test_get_num_list(self):
        a = self.v._get_user_input("dgd 23 45",
                                   "",
                                   "23 45",
                                   "dgd 23 45",
                                   "34 534 dgdf",
                                   "jjg regh",
                                   "34.4",
                                   "3",
                                   "1 2 5 8 22-27",
                                   "dgd 23.3 45.3",
                                   "",
                                   "23.3 45",
                                   "dgd 23 45",
                                   "34 534 dgdf",
                                   "jjg regh",
                                   "45.2-56.7",
                                   "34.4",
                                   "3",)

        self.v._get_user_input = lambda: a

        # list of integers

        self.assertEqual(self.v._get_num_list(int), [])
        self.assertEqual(self.v._get_num_list(int), [23, 45])
        self.assertEqual(self.v._get_num_list(int), [3])
        self.assertEqual(self.v._get_num_list(int), [1,2,5,8,22,23,24,25,26,27])
        self.assertEqual(self.v._get_num_list(float), [])
        self.assertEqual(self.v._get_num_list(float), [23.3, 45.0])
        self.assertEqual(self.v._get_num_list(float), [34.4])

    def test_get_integer_in_range(self):
        a = self.v._get_user_input('8',
                                   '-19',
                                   '56-78',
                                   'good luck with that',
                                   '5',
                                   '-3',
                                   )

        self.v._get_user_input = lambda: a

        # testing values that should be in certain range
        self.assertEqual(self.v.prompt_for_integer_in_range(-3, 5), 5)
        self.assertEqual(self.v.prompt_for_integer_in_range(-3, 5), -3)

    def test_prompt_for_list_of_integers_in_range(self):
        a = self.v._get_user_input('12 1-5 22',
                                   '3 9-6 18 48',
                                   '3 1-8 18 28',
                                   )

        self.v._get_user_input = lambda: a

        self.assertEqual(self.v.prompt_for_list_of_integers_in_range(1, 22),
                         [12, 1, 2, 3, 4, 5, 22])

        self.assertEqual(self.v.prompt_for_list_of_integers_in_range(1, 28),
                         [3, 1, 2, 3, 4, 5, 6, 7, 8, 18, 28])

    def test_prompt_for_string_suitable_for_filename(self):
        a = self.v._get_user_input(
                'some_mail@with',
                'a24515',
                'spaced word',
                'word',
                'underscores_in_word',
                '',
                'dashed-word',
                '!',
                '#',
                'ra$#',
                '/',
                "d'Artanian",
                'multiple matches, should not match',
                )
        self.v._get_user_input = lambda: a

        self.assertEquals(self.v.prompt_for_string_suitable_for_filename(), 'a24515')
        self.assertEquals(self.v.prompt_for_string_suitable_for_filename(), 'spaced word')
        self.assertEquals(self.v.prompt_for_string_suitable_for_filename(), 'word')
        self.assertEquals(self.v.prompt_for_string_suitable_for_filename(), 'underscores_in_word')
        self.assertEquals(self.v.prompt_for_string_suitable_for_filename(), 'dashed-word') 

    def test_prompt_yes_no(self):
        a = self.v._get_user_input(
                'a',
                'whatever',
                '0',
                'n',
                'no',
                'f',
                'false',
                'off',
                'N',
                'F',
                'FalSE',
                '1',
                'y',
                'yes',
                'true',
                'on',
                't')
        self.v._get_user_input = lambda: a
        self.assertFalse(self.v.prompt_yes_no())
        self.assertFalse(self.v.prompt_yes_no())
        self.assertFalse(self.v.prompt_yes_no())
        self.assertFalse(self.v.prompt_yes_no())
        self.assertFalse(self.v.prompt_yes_no())
        self.assertFalse(self.v.prompt_yes_no())
        self.assertFalse(self.v.prompt_yes_no())
        self.assertFalse(self.v.prompt_yes_no())
        self.assertFalse(self.v.prompt_yes_no())

        self.assertTrue(self.v.prompt_yes_no())
        self.assertTrue(self.v.prompt_yes_no())
        self.assertTrue(self.v.prompt_yes_no())
        self.assertTrue(self.v.prompt_yes_no())
        self.assertTrue(self.v.prompt_yes_no())
        self.assertTrue(self.v.prompt_yes_no())
        

class TestRange(unittest.TestCase):

    def test_value_in_range(self):
        valid_range = Range(0, 7)
        self.assertTrue(4 in valid_range)
        self.assertTrue(0 in valid_range)
        self.assertTrue(7 in valid_range)
        self.assertFalse(-4 in valid_range)
        self.assertFalse(10 in valid_range)

        with self.assertRaises(RangeError):
            Range(8, 3)


if __name__== "__main__":
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(GetValuesTest))
    unittest.TextTestRunner(verbosity=2).run(suite)
