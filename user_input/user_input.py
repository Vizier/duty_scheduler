# A small and pretty naive library for validating user input
# handy for console apps. This version is for Python 3
import re
from distutils.util import strtobool


class UserInput(object):
    """ Responsible for getting values from user input.

    inp_msg: message for user inviting to input some data
    err_msg: message for user printed in case of invalid input
    testing: flag to disable printing messages during testing
    """
    def __init__(self, inp_msg="Please provide an input: ", 
                       err_msg="Wrong input: ",
                       testing=False):
        self.inp_msg = inp_msg
        self.err_msg = err_msg
        self.testing = testing

    def prompt_for_integer(self):
        """ Tries to get integer from stdin.

        Displays inp_msg.
        Displays err_msg if input is not valid integer.
        Prompts user for input until valid input provided.
        """
        return self._get_number(int)

    def prompt_for_float(self):
        """ Tries to get float from stdin.

        Displays inp_msg.
        Displays err_msg if input is not valid float.
        Prompts user for input until valid input provided.
        """
        return self._get_number(float)

    def prompt_for_integer_list(self):
        """ Tries to get a list of integers separated by whitespaces from stdin.

        Also supports a range of integers
        separated by dash (like "1 2 3 7-9" => [1,2,3,7,8,9])

        Displays inp_msg.
        Displays err_msg if input is not a list of integers separated by whitespaces.
        Prompts user for input until valid input provided.
        """
        return self._get_num_list(int)

    def prompt_for_float_list(self):
        """ Tries to get a list of floats separated by whitespace from stdin.

        Displays inp_msg.
        Displays err_msg if input is not a list of floats separated by whitespaces.
        Prompts user for input until valid input provided.
        """
        return self._get_num_list(float)

    def prompt_for_integer_in_range(self, low, high):
        valid_range = Range(low, high)
        while True:
            input_ = self.prompt_for_integer()
            if input_ in valid_range:
                return input_
            else:
                if not self.testing: print(self.err_msg, input_)

    def prompt_for_list_of_integers_in_range(self, low, high):
        valid_range = Range(low, high)
        while True:
            input_ = self.prompt_for_integer_list()
            if all((el in valid_range) for el in input_):
                return input_
            else:
                if not self.testing: print(self.err_msg, input_)

    def prompt_for_string_suitable_for_filename(self):
        """ Tries to get a string that can be used as a file
        name (part before file type extension).

        """
        regexp = r'[\w,\s-]+'
        while True:
            input_ = next(self._get_user_input())
            match = re.findall(regexp, input_)
            if len(match) == 1 and match[0] == input_:
                return input_
            else:
                if not self.testing: print(self.err_msg, input_)

    def prompt_yes_no(self):
        print('{} [y/n]'.format(self.inp_msg))
        while True:
            try:
                return strtobool(next(self._get_user_input()).lower())
            except ValueError:
                print('Please respond with \'y\' or \'n\'.')

    def _get_number(self, num):
        while True:
            input_ = next(self._get_user_input())
            try:
                return num(input_)
            except ValueError:
                if not self.testing: print(self.err_msg, input_)

    def _get_num_list(self, num):
        """
        Returns a list of numbers if user string can be represented so
        """
        while True:
            res = []
            l = self._get_list()
            try:
                for part in l:
                    if '-' in part and num == int:
                        res += self._expand_dashed_range(part)
                    else:
                        res.append(num(part))

                return res

            except (ValueError, TypeError):
                if not self.testing: print(self.err_msg, part)

    def _expand_dashed_range(self, part):
        """ '44-48' -> [44, 45, 46, 47, 48]
        """
        x = part.split('-')
        return list(range(int(x[0]), int(x[-1]) + 1))

    def _get_list(self):
        while True:
            return next(self._get_user_input()).strip().split()

    def _get_user_input(self):
        while True:
            yield input(self.inp_msg)


class Selector(object):
    """ Displays a group of numbered elements and prompts user to select one of them. 

    Provides public methods to get choice number or chosen element.
    
    """
    def __init__(self, title_msg, *args):
        print()
        print(title_msg)
        print()
        self._args = args
        self._size = len(self._args)
        self._err_msg = "Please enter number from 1 to {}: ".format(self._size)
        self._ui = UserInput(inp_msg="Your choice: ", err_msg=self._err_msg)
        print(self)
        # self.choice_number = self.get_choice()


    def __str__(self):
        return '     '.join(["{}: {}".format(i + 1, arg)
                         for i, arg in enumerate(self._args)])

    def get_choice_number(self):
        return self._ui.prompt_for_integer_in_range(1, self._size)

    def get_choice(self):
        return self._args[self.get_choice_number() - 1]


class RangeError(Exception):
    pass


class Range(object):
    """ Represents an interval between low and high.

    Allows to check if value is between low and high
    both for integer and float (should be used with caution,
    as always with floats).
    """

    def __init__(self, low, high):
        self._low = low
        self._high = high
        self._validate_bounds()

    def _validate_bounds(self):
        if self._low > self._high:
            raise RangeError("Lower bound is greater then high bound: {} > {}".
            format(self._low, self._high))

    def __contains__(self, item):
        return self._low <= item <= self._high

    def __str__(self):
        return "[{}, {}]".format(self._low, self._high)

if __name__=='__main__':
    gv = UserInput()
    print(gv.prompt_for_integer_list())

